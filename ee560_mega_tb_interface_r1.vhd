----------------------------------------------------
-- File: ee560_mega_tb_interface_r1.vhd (based on ee560_mega_tb_interface.vhd)
-- Mega Test Bench for INTERFACE design lab  
--	Designed by Gandhi Puvvada
--	Date:7/23/2011, revised on 7/25/2013 
----------------------------------------------------
-- Skip this long comment.
-- The following discussion about differences between the current and previous testbenches
-- is for the teaching team members. Students can skip this. 
-- This revision #1 differs from the original design (ee560_mega_tb_interface.vhd of July 2011) 
--    in this crucial aspect narrated below. 
-- The earlier design had fictitious testbench processes
--		to emulate the producer and the consumer latencies.
--		The earlier "Sender_Ctrl" producer process (now commented out) 
--		waits for the negative edge and then waits further for 80% of the remaining 
--		half of the clock and then provides the data at 90% of the clock.
--		This is the shortest latency.
-- The question is "90% of which clock?
--		If the "please_produce" is made active in the first half of the clock, 
--		then it responds in that *very* clock itself 
--		and if the request is made in a mealy fashion after the negative edge 
--		(i.e. after the 50% point)(say, at around 70% of the clock when the the 
--		synchronized signals settle down), it responds in the *next* clock at 90%.
-- 		This behavior is strange and is not realistic (not realistic because no 
--		hardware behaves like that.
-- A more realistic hardware (that responds in the same clock as the request is raised), 
--		is an asynchronous memory or register array with access time equivalent to 90% 
--		of the clock.
--		A CAM (content addressable memory like the register array with comparison units 
--		in our "divider with cache" can provide you hit either in the same clock 
--		if it is a hit or in a subsequent clock if it is a miss.
--		The "access time equivalent to 90% of the clock" can also be criticised 
--		because how can it track the clock period. Well, we can provide a fitting 
--		argument stating that the clock period matches the technology previaling 
--		in that era and similarly the memory array speeds also track the technology.
--      So what happens until the 90% of the clock time? 
--      The memory outputs "XXXX" for data and keeps PRODUCED response inactive 
--		(inactive in this testbench but unknown/unreliable so far as the STA 
--		(Static Timing Analyser) is concerned).
-- But do we want to go this route? If producer takes 90% of the clock to produce, 
--		then we wish to drop off the data in a register or a FIFO in the *same* 
--		clock domain first before further attempting to send it to *another* clock 
--		domain. Really? What is the difference between *same* clock domain and 
--		*different* clock domain? In both cases, you are dropping it in a FIFO 
--		(on the producer-side of the FIFO working at WCLK, so it does not matter, right? 
--		Yes, we want to arrive at that conclusion and say that the the FIFO solution 
--		is the right and robust preferred method and other ad hoc methods are bad.
-- Can we let the producer hold the produced data "in his hands" until we collect the data? 
--		It is possible in our "Divider with CAM" lab as long as we keep the inputs same.
--		Here we assume that the Producer is a BRAM based FIFO emulating the behavior 
--		of a variable latency producer. When you go through the code below, 
--		(particularly the two processes, Sender_2_state_SM and OFL_Sender_2_SM_comb_process),
--		you notice that the following "if" condition is satisfied at the beginning
--		of a clock. It could be the first clock *after* the clock in which "please_produce"
--		went active or a subsequent clock if the latency is higher.
--			((pp_pending_req = '1') and (producer_latency = P_count))
--		The following line will hold the data as long as P_index_registered does not change.
-- 		datain      <= "XXXX", producer_data_array(CONV_INTEGER(unsigned(P_index_registered))) after (1.8 * Tw); -- at 90% of the clock   
--		And P_index_registered will not change unless there is a new please_produce request.
-- What are these  P_OVERLAP   and    C_OVERLAP?
--		Some of the 13 Interface Designs were not done thoughtfully and did not 
--		try to exploit the pipelined nature of the Producer and the Consumer. 
--		Here, in our testbench, the producer will interpret an active  
--		"please_produce" at the end of the clock, when he (the producer) just
--		responded with "produced", as a *new* request to produce. This pipelined
--		action is referred to as a "overlapped" action. If the interface design 
--		somewhat simplistic can not exploit/handle this pipelined behavior, then 
--		he should send this testbench a '0' for P_OVERLAP (and similarly 
--		a '0' for C_OVERLAP). Notice how these (P_overlap and C_overlap) are used
--		in the code below.

----------------------------------------------------

----------------------------------------------------
library ieee, std;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
-- use ieee.std_logic_unsigned.all; -- use of this package is discouraged by some in the industry. 
-- So, without this, we need to write "unsigned(wp) - unsigned(rp)" inplace of simple "wp - rp".

entity tb_interface is
	generic( Tw : time :=  15 ns; -- half cycle time
			 Tr : time :=  11 ns );
end tb_interface;

architecture arc of tb_interface is

	component interface
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
	end component;
	
	-- some naming convention used here: P_ = Producer_, C_ = Consumer_, pp_ = please_produce_, pc_ = please_consume_
	signal produced, consumed: std_logic:= '0';
	signal please_produce, please_consume: std_logic;
	signal wclk, rclk, resetb: std_logic := '1';
	signal datain, dataout, dataout_registered: std_logic_vector(3 downto 0);
	signal producer_done, consumer_done: std_logic := '0';
	signal producer_latency, P_count, P_index, P_index_registered: std_logic_vector(3 downto 0);
	signal P_last_item_just_processed: std_logic; -- It is set during the processing of the last item (when P_index = 15) 
	signal pp_pending_req, produced_int: std_logic; 
	signal P_overlap: std_logic := '0'; 
	signal consumer_latency, C_count, C_index, C_index_registered: std_logic_vector(3 downto 0);
	signal C_last_item_just_processed: std_logic; -- It is set during the processing of the last item (when C_index = 15)
	signal pc_pending_req, consumed_int: std_logic; 
	signal C_overlap: std_logic := '0'; 
	

subtype reg is std_logic_vector (3 downto 0);
-- Type definition of 16-latencies register array and also 16 4-bit data items
type reg_array is array (0 to 15) of reg;
constant producer_latency_array : reg_array :=  -- minimum latency = 0 after registering the request
              ( X"3",     --0          -- which guarantees the 1 clock delay due to BRAM
                X"5",     --1   
                X"0",     --2
                X"0",     --3      -- however, the BRAM works like a pipeline
                X"0",     --4      -- if the latency is continuously 0 and if 
                X"0",     --5      -- please_produce request is true continuously. 
                X"0",     --6
                X"5",     --7
                X"4",     --8
                x"9",     --9
                x"2",     --10
                X"7",     --11
                X"0",     --12
                X"0",     --13
                X"A",     --14
                X"7"      --15
              );
constant producer_data_array : reg_array :=  -- the 16 data items are 0 to F in that order
              ( X"0",     --0                -- This is for simplicity.
                X"1",     --1   
                X"2",     --2
                X"3",     --3      
                X"4",     --4       
                X"5",     --5       
                X"6",     --6
                X"7",     --7
                X"8",     --8
                x"9",     --9
                x"A",     --10
                X"B",     --11
                X"C",     --12
                X"D",     --13
                X"E",     --14
                X"F"      --15
              );

constant consumer_latency_array : reg_array :=  -- minimum latency = 0 after registering the request
              ( X"0",     --0          -- which guarantees the 1 clock delay due to BRAM
                X"5",     --1   
                X"0",     --2
                X"0",     --3      -- however, the BRAM works like a pipeline
                X"0",     --4      -- if the latency is continuously 0 and if 
                X"0",     --5      -- please_produce request is true continuously. 
                X"0",     --6
                X"5",     --7
                X"0",     --8
                x"9",     --9
                x"2",     --10
                X"7",     --11
                X"0",     --12
                X"0",     --13
                X"7",     --14
                X"A"      --15
              );

signal consumer_data_array : reg_array :=  -- the 16 data items are made unknown
              ( "XXXX",     --0                -- so that our design overwrites them with correct values.
                "XXXX",     --1   
                "XXXX",     --2
                "XXXX",     --3      
                "XXXX",     --4       
                "XXXX",     --5       
                "XXXX",     --6
                "XXXX",     --7
                "XXXX",     --8
                "XXXX",     --9
                "XXXX",     --10
                "XXXX",     --11
                "XXXX",     --12
                "XXXX",     --13
                "XXXX",     --14
                "XXXX"      --15
              );
			  
----------------------------------------------------------------------------------	
begin -- begin of the architecture


	u_interface: interface port map( 
		RESETB => resetb, WCLK => wclk , RCLK => rclk,
		PRODUCED => produced,  CONSUMED => consumed,
		DATAIN => datain, DATAOUT => dataout,
		PLEASE_PRODUCE => please_produce,  PLEASE_CONSUME => please_consume,
		P_OVERLAP => P_overlap, C_OVERLAP => C_overlap);
	
	wclk <= not wclk after Tw;

	rclk <= not rclk after Tr;

	resetb <= '0', '1' after (2.1)*Tw; -- 2*Tw changed to (2.1)*Tw


-------------------------------------------------------------------------------
OFL_Sender_2_SM_comb_process:
	process (pp_pending_req, producer_latency, P_count, P_index)
	begin
      if ((pp_pending_req = '1') and (producer_latency = P_count)) then
          produced_int <= '1'after (1.8 * Tw);      
      else 
          produced_int <= '0' after (0.2 * Tw);
      end if;
	end process OFL_Sender_2_SM_comb_process;

	produced <= produced_int ; -- PRODUCED port is assigned with internal produced_int
	-- datain_int <= producer_data_array(CONV_INTEGER(unsigned(P_index_registered)));
	
	-- datain      <= "XXXX", producer_data_array(CONV_INTEGER(unsigned(P_index_registered))) after (1.8 * Tw); -- at 90% of the clock
-------------------------------------------------------------------------------
	Sender_2_state_SM: process (wclk, resetb) -- sender's 2-state state machine with pp_pending_req as the state variable
	file sent_data: text open write_mode is "ee560_sent_data.txt";
	file sent_data_with_time: text open write_mode is "ee560_sent_data_with_time.txt";
	variable outline1, outline2 : line;
	constant time_string: string (1 to 9) := "  time = ";	
	begin
		if (resetb = '0') then
			pp_pending_req <= '0'; 
			P_count <= "0000";
			P_index <= "0000";
			producer_done <= '0';
			P_last_item_just_processed <= '0';
		elsif (wclk'event AND wclk = '1') then
		
		    case pp_pending_req is -- The pp_pending_req means "please_produce" related pending request with two state values: 0 and 1
  
				when '0' =>
				  if ((please_produce = '1') AND (P_last_item_just_processed = '0')) then
					  pp_pending_req <= '1';
					  producer_latency <= producer_latency_array (CONV_INTEGER(unsigned(P_index)));
					  P_count <= "0000";
					  P_index_registered <= P_index ;
					  if (producer_latency_array (CONV_INTEGER(unsigned(P_index))) = X"0") then
						datain      <= "XXXX", producer_data_array(CONV_INTEGER(unsigned(P_index))) after (1.8 * Tw); -- at 90% of the next clock -- Notice the usage of P_index (and not P_index_registered)
						write( outline1, producer_data_array(CONV_INTEGER(unsigned(P_index))));
						write( outline2, producer_data_array(CONV_INTEGER(unsigned(P_index))));
					  else
						datain      <= "XXXX";
					  end if;
					  if (P_index = "1111") then
						P_last_item_just_processed <= '1';
					  end if;
					  P_index <= unsigned(P_index) + '1';
				  end if;
				  
				when others =>  -- i.e. when  '1' => 	
				  if (produced_int = '1') then
					writeline(sent_data, outline1);
					write(outline2, time_string);
					write(outline2, now);
					writeline(sent_data_with_time, outline2);
				  end if;	
				  if ( (produced_int = '1') AND  (please_produce = '1') AND (P_overlap = '1') AND (P_last_item_just_processed = '0') )  then
					  producer_latency <= producer_latency_array (CONV_INTEGER(unsigned(P_index)));
					  P_count <= "0000"; 
					  P_index_registered <= P_index ; 
					  if (producer_latency_array (CONV_INTEGER(unsigned(P_index))) = X"0") then
						datain      <= "XXXX", producer_data_array(CONV_INTEGER(unsigned(P_index))) after (1.8 * Tw); -- at 90% of the next clock -- Notice the usage of P_index (and not P_index_registered)
						write( outline1, producer_data_array(CONV_INTEGER(unsigned(P_index))));
						write( outline2, producer_data_array(CONV_INTEGER(unsigned(P_index))));
					  else
						datain      <= "XXXX";
					  end if;
					  if (P_index = "1111") then
						P_last_item_just_processed <= '1';
					  end if;
					  P_index <= unsigned(P_index) + '1';
				  else
					  P_count <= unsigned(P_count) + '1';  
					  if (unsigned(P_count) + '1' = producer_latency) then  -- notice the " + '1' ". Since the datain is registered, for it not to be late, you want to register it at the end of the previous clock.
						datain      <= "XXXX", producer_data_array(CONV_INTEGER(unsigned(P_index_registered))) after (1.8 * Tw); 
						write( outline1, producer_data_array(CONV_INTEGER(unsigned(P_index_registered))));
						write( outline2, producer_data_array(CONV_INTEGER(unsigned(P_index_registered))));
					  end if;
				  end if;
				  if ( (produced_int = '1') AND  (datain = "1111")  ) then
					producer_done <= '1';
				    assert( false ) report "Last Data Dispatched !!"
					   severity note;
				  end if;
				  if ((please_produce = '0' ) OR ( (produced_int = '1') AND ((P_overlap = '0') OR ((P_last_item_just_processed = '1')) ) ) ) then
					 pp_pending_req <= '0';
				  end if;
		  
			end case;
	   
		end if;
		
	end process; -- end of Sender_2_state_SM


-------------------------------------------------------------------------------
OFL_Receiver_2_SM_comb_process:
   process (pc_pending_req, consumer_latency, C_count, C_index)
   begin
      if ((pc_pending_req = '1') and (consumer_latency = C_count)) then
          consumed_int <= '1' after (1.8 * Tr);      
		  --  consumer_data_array(CONV_INTEGER(unsigned(C_index)))  <= dataout; -- do this in a clocked process
      else 
          consumed_int <= '0' after (0.2 * Tr);

      end if;
	end process OFL_Receiver_2_SM_comb_process;

	consumed <= consumed_int ; -- CONSUMED port is assigned with internal consumed_int
	
-------------------------------------------------------------------------------
	Receiver_2_state_SM: process (rclk, resetb) -- receiver's 2-state state machine with pc_pending_req as the state variable
	file received_data: text open write_mode is "ee560_received_data.txt";
	file received_data_with_time: text open write_mode is "ee560_received_data_with_time.txt";
	variable outline : line;
	constant time_string: string (1 to 9) := "  time = ";
	begin
		if (resetb = '0') then
			pc_pending_req <= '0'; 
			C_count <= "0000";
			C_index <= "0000";
			consumer_done <= '0';
			C_last_item_just_processed <= '0';
		elsif (rclk'event AND rclk = '1') then
		
		    case pc_pending_req is -- The pc_pending_req means "please_consume" related pending request with two state values: 0 and 1
  
				when '0' =>
				  if ( (please_consume = '1') AND (C_last_item_just_processed = '0') ) then
					  pc_pending_req <= '1';
					  consumer_latency <= consumer_latency_array (CONV_INTEGER(unsigned(C_index)));
					  C_count <= "0000";
					  C_index_registered <= C_index ; -- register the address 
					  if (C_index = "1111") then
						C_last_item_just_processed <= '1';
					  end if;
					  dataout_registered <= dataout ; -- register the data
					  C_index <= unsigned(C_index) + '1';
				  end if;
				  
				when others =>  -- i.e. when  '1' =>
				  if ( (consumed_int = '1') AND  (please_consume = '1') AND (C_overlap = '1') AND (C_last_item_just_processed = '0') )  then
					  consumer_latency <= consumer_latency_array (CONV_INTEGER(unsigned(C_index)));
					  C_count <= "0000";
					  C_index_registered <= C_index ; -- register the address 
					  if (C_index = "1111") then
						C_last_item_just_processed <= '1';
					  end if;
					  dataout_registered <= dataout ; -- register the data
					  C_index <= unsigned(C_index) + '1';
				  else
					  C_count <= unsigned(C_count) + '1';  
				  end if;
				  if (consumer_latency = C_count) then
					consumer_data_array(CONV_INTEGER(unsigned(C_index_registered)))  <= dataout_registered;
					write( outline, dataout_registered);
					writeline(received_data, outline);
					write(outline, dataout_registered);
					write(outline, time_string);
					write(outline, now);
					writeline(received_data_with_time, outline);
				  end if;
				  if ( (consumed_int = '1') AND  (dataout_registered = "1111")  ) then
					consumer_done <= '1';
					assert( false ) report "Last Data Received !!"
						severity note;
				  end if;
				  if ((please_consume = '0' ) OR ( (consumed_int = '1') AND ( (C_overlap = '0') OR (C_last_item_just_processed = '1') ) ) ) then
					 pc_pending_req <= '0';
				  end if;
		  
			end case;
	   
		end if;
		
	end process;  -- end of Receiver_2_state_SM
-------------------------------------------------------------------------------
		
	response_recording_process: process (producer_done, consumer_done)
	
	file response_record: text open append_mode is "ee560_interface_mega_responses_r1.txt";
	variable outline : line;
	
	begin
	
	if (producer_done'event and producer_done = '1') then 
		write(outline, string'("  ")); -- Leading Blank line
		writeline(response_record, outline);
		write(outline, string'("Last data was dispatched by the Producer ! Time = "));
		write (outline, now);
		writeline(response_record, outline);
	end if;
	
	if (consumer_done'event and consumer_done = '1') then 
		write(outline, string'("Last data was received by the Consumer !! Time = "));
		write (outline, now);
		writeline(response_record, outline);
		write(outline, string'("  ")); -- Trailing Blank line
		writeline(response_record, outline); 
		
	end if;
	
	-- files are closed every time a process such as this 
	-- terminates and are reopened every time this process is invoked.
	
	end process;

end arc;

-- Students can skip reading this.
--  July 2011 "Sender_Ctrl" producer process. This is not realistic. So we replaced it by the above design in July 2013.

	-- Sender_Ctrl: process
		-- file producer_data: text open read_mode is "ee560_producer_data.txt";
		-- variable inline : line;
		-- variable data_send : std_logic_vector(3 downto 0);
		-- variable send_cycle: integer;
	-- begin
		-- producer_done <= '0';
		-- wait until resetb ='1';

		-- while not endfile( producer_data) loop
		-- if(please_produce = '1') then
			-- datain <= (others => 'Z');
			-- produced <= '0';
			-- readline(producer_data, inline);
			-- read(inline, send_cycle);
			-- read(inline, data_send);
			-- for i in 1 to send_cycle loop
				-- wait until wclk'event and wclk = '0';
			-- end loop;
			-- wait for Tw*0.8;
			-- datain <= data_send;
			-- produced <= '1';
		-- end if;
		-- wait until wclk'event and wclk = '1';
		-- wait for 1 ns;
		-- end loop;

		-- produced <= '0';
		-- assert (false) report "End of Producer File"
		-- severity note;
		
		-- producer_done <= '1'; -- to signal recording response
		
		-- wait;
	-- end process;
