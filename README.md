# Interface_Lab

Implemented various CDC mechanism such as 4-way and 2-way asynchronous handshake in VHDL finally proved that 2 clock FIFO is the best way for CDC. Designed and Simulated FIFO using Register-Arrays, Flow-Through SSRAM and Pipelined SSRAM in VHDL and also allowed one data per clock throughout in spite of the presence of input and output registers in the SSRAMs.