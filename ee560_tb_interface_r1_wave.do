onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_interface/resetb
##############################################################################################################
add wave -noupdate -divider Producer
add wave -noupdate /tb_interface/wclk
add wave -noupdate /tb_interface/please_produce
add wave -noupdate /tb_interface/produced
add wave -noupdate -radix hexadecimal /tb_interface/datain
add wave -noupdate /tb_interface/producer_done
add wave -noupdate /tb_interface/pp_pending_req
add wave -noupdate -radix hexadecimal /tb_interface/producer_latency
add wave -noupdate -radix hexadecimal /tb_interface/P_count
add wave -noupdate -radix hexadecimal /tb_interface/P_index
add wave -noupdate -radix hexadecimal /tb_interface/P_index_registered
add wave -noupdate /tb_interface/P_last_item_just_processed
add wave -noupdate /tb_interface/P_overlap
##############################################################################################################
add wave -noupdate -divider Consumer
add wave -noupdate /tb_interface/rclk
add wave -noupdate /tb_interface/please_consume
add wave -noupdate /tb_interface/consumed
add wave -noupdate -radix hexadecimal /tb_interface/dataout
add wave -noupdate -radix hexadecimal /tb_interface/dataout_registered
add wave -noupdate /tb_interface/consumer_done
add wave -noupdate /tb_interface/pc_pending_req
add wave -noupdate -radix hexadecimal /tb_interface/consumer_latency
add wave -noupdate -radix hexadecimal /tb_interface/C_count
add wave -noupdate -radix hexadecimal /tb_interface/C_index
add wave -noupdate -radix hexadecimal /tb_interface/C_index_registered
add wave -noupdate /tb_interface/C_last_item_just_processed
add wave -noupdate /tb_interface/C_overlap
add wave -noupdate -radix hexadecimal -childformat {{/tb_interface/consumer_data_array(0) -radix hexadecimal} {/tb_interface/consumer_data_array(1) -radix hexadecimal} {/tb_interface/consumer_data_array(2) -radix hexadecimal} {/tb_interface/consumer_data_array(3) -radix hexadecimal} {/tb_interface/consumer_data_array(4) -radix hexadecimal} {/tb_interface/consumer_data_array(5) -radix hexadecimal} {/tb_interface/consumer_data_array(6) -radix hexadecimal} {/tb_interface/consumer_data_array(7) -radix hexadecimal} {/tb_interface/consumer_data_array(8) -radix hexadecimal} {/tb_interface/consumer_data_array(9) -radix hexadecimal} {/tb_interface/consumer_data_array(10) -radix hexadecimal} {/tb_interface/consumer_data_array(11) -radix hexadecimal} {/tb_interface/consumer_data_array(12) -radix hexadecimal} {/tb_interface/consumer_data_array(13) -radix hexadecimal} {/tb_interface/consumer_data_array(14) -radix hexadecimal} {/tb_interface/consumer_data_array(15) -radix hexadecimal}} -subitemconfig {/tb_interface/consumer_data_array(0) {-radix hexadecimal} /tb_interface/consumer_data_array(1) {-radix hexadecimal} /tb_interface/consumer_data_array(2) {-radix hexadecimal} /tb_interface/consumer_data_array(3) {-radix hexadecimal} /tb_interface/consumer_data_array(4) {-radix hexadecimal} /tb_interface/consumer_data_array(5) {-radix hexadecimal} /tb_interface/consumer_data_array(6) {-radix hexadecimal} /tb_interface/consumer_data_array(7) {-radix hexadecimal} /tb_interface/consumer_data_array(8) {-radix hexadecimal} /tb_interface/consumer_data_array(9) {-radix hexadecimal} /tb_interface/consumer_data_array(10) {-radix hexadecimal} /tb_interface/consumer_data_array(11) {-radix hexadecimal} /tb_interface/consumer_data_array(12) {-radix hexadecimal} /tb_interface/consumer_data_array(13) {-radix hexadecimal} /tb_interface/consumer_data_array(14) {-radix hexadecimal} /tb_interface/consumer_data_array(15) {-radix hexadecimal}} /tb_interface/consumer_data_array
##############################################################################################################
add wave -noupdate -divider UUT
add wave -noupdate -group {UUT}
add wave -group {UUT} sim:/tb_interface/u_interface/*
##############################################################################################################
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1 us} 0} {{Cursor 2} {2 us} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 100
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2400 ns}
