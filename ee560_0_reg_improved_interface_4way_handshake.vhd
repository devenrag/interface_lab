----------------------------------------------------
-- File: ee560_0_reg_improved_interface_4way_handshake.vhd
--  0 register improved INTERFACE design 4-way handshake
--
--	Designed by Gandhi Puvvada and Lung-Sheng Chen	
--	Date:12/1/99, 7/20/2006, 7/20/2011, 7/19/2015
----------------------------------------------------

----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal take, got,take_s, got_s, please_consume_i: std_logic;
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;

	type W_state_enum is ( QW_produce, QW_filled, QW_wait);
	type R_state_enum is ( QR_consume, QR_read, QR_wait);
	signal wstate, n_wstate : W_state_enum;
	signal rstate, n_rstate: R_state_enum;

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded interface design -- no other better choice for a 0-reg case
	
----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;


----  State Control---------------------

	W_State_Ctrl: process(PRODUCED, wstate, got_s)
	begin
	n_wstate <= wstate;
	PLEASE_PRODUCE <= '0';

	case wstate is  
		when QW_produce =>
			take <= '0';
			PLEASE_PRODUCE <= '1';
			if( PRODUCED = '1' and got_s = '0') then 
				n_wstate <= QW_filled;
			elsif( PRODUCED = '1' and got_s = '1') then
				n_wstate <= QW_wait;
			end if;
		when QW_wait =>
			take <= '0';
			if( got_s = '0' ) then
				n_wstate <= QW_filled; 
			end if;
		when QW_filled =>
			take <= '1';
			if( got_s = '1'  ) then
				n_wstate <= QW_produce;
			end if;
		when others => null;
	end case;
	end process;


	W_State_Reg:process(WCLK, w_syn_resetb)
	begin
		if( w_syn_resetb = '0') then
			wstate <= QW_produce;
		elsif( WCLK'event and WCLK = '1') then
			wstate <= n_wstate;
		end if;
	end process;


	R_State_Ctrl:process(CONSUMED, rstate, take_s)
	begin
	n_rstate <= rstate;
	please_consume_i <= '0';
	
	case rstate is 
		when QR_wait =>
			got <= '0';
			if( take_s ='1') then 
				n_rstate <= QR_consume;
			end if;
		when QR_consume=>
			got <= '0';
			please_consume_i <= '1';
			if( CONSUMED = '1') then
				n_rstate <= QR_read;
			end if;
		when QR_read =>
			got <= '1';
			if( take_s ='0' ) then
				n_rstate <= QR_wait;
			end if;
		when others => null;
	end case;
	end process;
	

	R_State_Reg:process(RCLK, r_syn_resetb)
	begin
		if( r_syn_resetb = '0') then
			rstate <= QR_wait;
		elsif( RCLK'event and RCLK = '1') then
			rstate <= n_rstate;
		end if;
	end process;


	Sampled_take:process(RCLK, r_syn_resetb)
	begin
		if( r_syn_resetb = '0') then
			take_s <= '0';
		elsif( RCLK'event and RCLK = '1') then
			take_s <= take;
		end if;
	end process;

	Sampled_got:process(WCLK, w_syn_resetb)
	begin
		if( w_syn_resetb = '0') then
			got_s <= '0';
		elsif( WCLK'event and WCLK = '1') then
			got_s <= got;
		end if;
	end process;

	PLEASE_CONSUME <= please_consume_i;

	DATAOUT <= DATAIN when please_consume_i = '1' else (others => 'Z');

end arc;
