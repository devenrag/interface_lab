----------------------------------------------------
--  File: ee560_0_reg_interface_2way_handshake_2states_the_FIFO_way.vhd (slightly different from ee560_0_reg_interface_2way_handshake_2states_one_way.vhd)  
--  
--  
--  
--  
--  

--	Designed by Gandhi Puvvada	
--	Date:7/29/2010, 7/21/2011, 7/19/2015
----------------------------------------------------

----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal take, got, n_take, n_got,  please_consume_i: std_logic;
	signal take_s, got_s, take_ss, got_ss, FULL, EMPTY: std_logic; -- change_in_take, change_in_got: std_logic;
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;
-- to distinguish from the previous 4 states with suffixes _1 and _2, let us use _0 here.
	type W_state_enum is (QW_produce_0, QW_take_0);
	type R_state_enum is (QR_wait_0, QR_consume_0);
--	type W_state_enum is (QW_produce_1, QW_take_1, QW_produce_2, QW_take_2);
--	type R_state_enum is (QR_wait_1, QR_consume_1, QR_wait_2, QR_consume_2);
	signal wstate, n_wstate : W_state_enum;
	signal rstate, n_rstate: R_state_enum;

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded interface design -- no other better choice for a 0-reg case
	
----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;


----  State Control---------------------

    -- change_in_got <= got_s XOR got_ss;
	FULL <= take XOR got_s;

	-- W_State_Ctrl: process(PRODUCED, wstate, change_in_got)
	W_State_Ctrl: process(PRODUCED, wstate, FULL)
	begin
	n_wstate <= wstate;
	PLEASE_PRODUCE <= '0';
    n_take <= take;
	
	case wstate is  
		when QW_produce_0 =>
			PLEASE_PRODUCE <= '1';
			if( PRODUCED = '1') then 
				n_wstate <= QW_take_0;
				n_take <= not take; -- flip the take FF on the next wclk edge
			end if;
		when QW_take_0 =>
			if (FULL = '0') then -- if( change_in_got = '1'  ) then
				n_wstate <= QW_produce_0;
			end if;
		when others => null;
	end case;
	end process;


	W_State_Reg:process(WCLK, w_syn_resetb)
	begin
		if( w_syn_resetb = '0') then
			wstate <= QW_produce_0;
			take <= '0';
		elsif( WCLK'event and WCLK = '1') then
			wstate <= n_wstate;
			take <= n_take;
		end if;
	end process;

    -- change_in_take <= take_s XOR take_ss;
	EMPTY <= take_s XNOR got;

	-- R_State_Ctrl:process(CONSUMED, rstate, change_in_take)
	R_State_Ctrl:process(CONSUMED, rstate, EMPTY)
	begin
	n_rstate <= rstate;
	please_consume_i <= '0';
	n_got <= got;
	
	case rstate is 
		when QR_wait_0 =>
			if (EMPTY = '0') then -- if( change_in_take ='1') then 
				n_rstate <= QR_consume_0;
			end if;
		when QR_consume_0 =>
			please_consume_i <= '1';
			if( CONSUMED = '1') then
				n_rstate <= QR_wait_0;
				n_got <= not got;
			end if;
		when others => null;
	end case;
	end process;
	

	R_State_Reg:process(RCLK, r_syn_resetb)
	begin
		if( r_syn_resetb = '0') then
			rstate <= QR_wait_0;
			got <= '0';
		elsif( RCLK'event and RCLK = '1') then
			rstate <= n_rstate;
			got <= n_got;
		end if;
	end process;


	Sampled_take:process(RCLK, r_syn_resetb)
	begin
		if( r_syn_resetb = '0') then
			take_s <= '0';
			take_ss <= '0';
		elsif( RCLK'event and RCLK = '1') then
			take_s <= take;
			take_ss <= take_s;
		end if;
	end process;

	Sampled_got:process(WCLK, w_syn_resetb)
	begin
		if( w_syn_resetb = '0') then
			got_s <= '0';
			got_ss <= '0';
		elsif( WCLK'event and WCLK = '1') then
			got_s <= got;
			got_ss <= got_s;
		end if;
	end process;

	PLEASE_CONSUME <= please_consume_i;

	DATAOUT <= DATAIN when please_consume_i = '1' else (others => 'Z');

end arc;
