----------------------------------------------------
-- File: ee560_1_reg_interface_r1.vhd 
--  1 register INTERFACE design
--
--	Designed by Gandhi Puvvada and Lung-Sheng Chen	
--	Date:12/1/99, 7/20/2006 (file name added), 7/21/2011 (a_write and a_read are made active-high)
----------------------------------------------------

----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is
	signal full, empty,almost_empty: std_logic;
	signal a_write, a_read, please_consume_i: std_logic;
	signal b_write, b_read: std_logic;
	signal a_reg: std_logic_vector(3 downto 0);
	signal b_reg: std_logic_vector(3 downto 0);
	signal g_rclk_a, g_wclk_a, a_filled, a_filled_s, a_empty, a_empty_s: std_logic;
	signal g_rclk_b, g_wclk_b,b_filled, b_filled_s, b_empty, b_empty_s: std_logic;
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;
	signal ae_presetb, ae_s_presetb, af_presetb: std_logic;
	signal be_presetb, be_s_presetb, bf_presetb: std_logic;
	signal ping_pong_write: std_logic  ;
	signal next_ping_pong_write: std_logic ;
	signal ping_pong_read: std_logic ;
	signal next_ping_pong_read: std_logic ;
	type W_state_enum is ( QW_produce, QW_filled, QW_wait);
	type R_state_enum is ( QR_consume, QR_read, QR_wait);
	signal wstate, n_wstate : W_state_enum;
	signal rstate, n_rstate: R_state_enum;

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded inerface design

----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
			
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;


----  State Control---------------------

	W_State_Ctrl: process(PRODUCED, wstate, full, ping_pong_write)   --DO I NEED TO INCLUDE ping_pong_write IN THE SENSITIVITY LIST?
	
	begin
			n_wstate <= wstate;
			a_write <= '0'; -- high-active signal inactivated by default
			b_write <= '0';
			next_ping_pong_write <= ping_pong_write;
			PLEASE_PRODUCE <= '0';
			case wstate is  
						when QW_produce =>
							PLEASE_PRODUCE <= '1';
							if( PRODUCED = '1' and full = '0') then 
								n_wstate <= QW_filled;
							elsif( PRODUCED ='1' and full = '1') then
								n_wstate <= QW_wait;
							end if;
						when QW_wait =>
							if( full = '0' ) then
								n_wstate <= QW_filled; 
							end if;
						when QW_filled =>
							next_ping_pong_write <= not (ping_pong_write);
							--ping_pong_write <= not (next_ping_pong_write);
							if (ping_pong_write = '0') then
								 a_write <= '1';
							elsif (ping_pong_write = '1') then 			
								b_write <= '1';	
							end if;
							n_wstate <= QW_produce;
							
						when others => null;
			end case;
	end process;




	W_State_Reg:process(WCLK, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				wstate <= QW_produce;
			   ping_pong_write <= '0'; --IF I INITIALE THESE TWO VARIABLES TO ZERO HERE THEN ON THE OUTPUT GRAPHS IT SHOWS RED LINE (UNDEFINED) HENCE I INITIALISED THEM WHILE DECLARING THE VARIABLE ABOVE 
				--next_ping_pong_write <= '0';
				--a_reg <= (others => '0');
				--b_reg <= (others => '0');
			elsif( WCLK'event and WCLK = '1') then
				wstate <= n_wstate;
				ping_pong_write <= next_ping_pong_write;
			end if;
	end process;


	R_State_Ctrl:process(CONSUMED, rstate,empty,almost_empty,ping_pong_read)
	begin
			n_rstate <= rstate;
			a_read <= '0'; -- high-active signal inactivated by default
			please_consume_i <= '0';
			b_read <= '0';
			next_ping_pong_read <= ping_pong_read;
			case rstate is 
						when QR_wait =>
							if( empty ='0') then 
								n_rstate <= QR_consume;
							end if;
						when QR_consume=>
							please_consume_i <= '1';
							if( CONSUMED = '1') then
								n_rstate <= QR_read;
							end if;
						when QR_read =>
						next_ping_pong_read <= not(ping_pong_read);
							if (ping_pong_read = '0') then
								a_read <= '1';
								
							elsif (ping_pong_read = '1') then 
								b_read <= '1';
								
							end if;
							if (almost_empty = '0') then
							--if (empty = '1') then
							n_rstate <= QR_wait;
							elsif (almost_empty = '1') then
							--elsif (empty = '0') then
							n_rstate <= QR_consume;
							end if;
						when others => null;
			end case;
	end process;
	

	R_State_Reg:process(RCLK, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				rstate <= QR_wait;
				ping_pong_read <= '0';
				--next_ping_pong_read <= '0';
			elsif( RCLK'event and RCLK = '1') then
				rstate <= n_rstate;
				ping_pong_read <= next_ping_pong_read;
			end if;
	end process;
	

----  Gated Clock---------------------
	
	g_wclk_a <= not ( a_write and (not WCLK) );
	g_rclk_a <= not ( a_read  and (not RCLK) );
	g_wclk_b <= not ( b_write and (not WCLK) );
	g_rclk_b <= not ( b_read  and (not RCLK) );
	
----  Data registers---------------------

	Data_Reg_a:process(g_wclk_a)
	begin
		if( g_wclk_a'event and g_wclk_a= '1') then
		--if (ping_pong_write = '0') then
			a_reg <= DATAIN;
			--ping_pong_write <= (next_ping_pong_write);
		--	end if;
		end if;
	end process;
	
	Data_Reg_b:process(g_wclk_b)
	begin
		if( g_wclk_b'event and g_wclk_b= '1') then
		--	if (ping_pong_write = '1') then
			b_reg <= DATAIN;
			--ping_pong_write <=  (next_ping_pong_write);
			--end if;
		end if;
	end process;

----  handshaking signals---------------------

	ae_presetb <= g_rclk_a and w_syn_resetb;
	A_empty_reg:process(g_wclk_a, ae_presetb)
	begin
			if( ae_presetb = '0') then
				a_empty <= '1';
			elsif( g_wclk_a'event and g_wclk_a= '1') then
				a_empty <= '0';
			end if;
	end process;
	
	be_presetb <= g_rclk_b and w_syn_resetb;
	B_empty_reg:process(g_wclk_b, be_presetb)
	begin
			if( be_presetb = '0') then
				b_empty <= '1';
			elsif( g_wclk_b'event and g_wclk_b= '1') then
				b_empty <= '0';
			end if;
	end process;
	
	
	ae_s_presetb <= g_rclk_a and r_syn_resetb;
	A_empty_s_reg:process(RCLK, ae_s_presetb)
	begin
			if( ae_s_presetb = '0') then
				 a_empty_s <= '1';
			elsif( RCLK'event and RCLK= '1') then
				 a_empty_s <= a_empty;
			end if;
	end process;
	
	be_s_presetb <= g_rclk_b and r_syn_resetb;
	B_empty_s_reg:process(RCLK, be_s_presetb)
	begin
			if( be_s_presetb = '0') then
				 b_empty_s <= '1';
			elsif( RCLK'event and RCLK= '1') then
				 b_empty_s <= b_empty;
			end if;
	end process;


	af_presetb <= g_wclk_a;
	A_filled_reg:process(g_rclk_a, af_presetb, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				a_filled <= '0';	
			elsif( af_presetb = '0') then
				 a_filled <= '1';
			elsif( g_rclk_a'event and g_rclk_a= '1') then
				a_filled <= '0';
			end if;
	end process;
	
	bf_presetb <= g_wclk_b;
	B_filled_reg:process(g_rclk_b, bf_presetb, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				b_filled <= '0';	
			elsif( bf_presetb = '0') then
				 b_filled <= '1';
			elsif( g_rclk_b'event and g_rclk_b= '1') then
				b_filled <= '0';
			end if;
	end process;
	
	
	A_filled_s_reg:process(WCLK, af_presetb, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				a_filled_s <= '0';	
			elsif( af_presetb = '0') then
				a_filled_s <= '1';
			elsif( WCLK'event and WCLK= '1') then
				 a_filled_s <= a_filled;
			end if;
	end process;
	
	B_filled_s_reg:process(WCLK, bf_presetb, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				b_filled_s <= '0';	
			elsif( bf_presetb = '0') then
				b_filled_s <= '1';
			elsif( WCLK'event and WCLK= '1') then
				 b_filled_s <= b_filled;
			end if;
	end process;

----  output signals---------------------
	
	PLEASE_CONSUME <= please_consume_i;
	
	full <= '1' when (a_filled_s = '1' and b_filled_s = '1') else '0';
	empty <= '1' when ((a_empty_s = '1' and b_empty_s = '1')) else '0';
	almost_empty <= '1' when ((a_empty_s = '0' and b_empty_s = '1') or (a_empty_s = '1' and b_empty_s = '0')) else '0';
	
	
	DATAOUT <= --a_reg when (please_consume_i = '1' )else
	 
			   --b_reg when (please_consume_i = '1')else
				a_reg when (please_consume_i = '1' and ping_pong_read = '0')else
	 
			   b_reg when (please_consume_i = '1' and ping_pong_read = '1')else
	  		  (others => 'Z');
				
				--ping_pong_read <=  (next_ping_pong_read);

end arc;
