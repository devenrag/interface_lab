-- ee560_1_reg_interface_1_loc_FIFO_with_prefetch.vhd
-- 7/23/2015

-- This includes improvement in activating PLEASE_PRODUCE in the last clock in the PREFETCHED state. 
-- This improvement is as per the solution for Q#2.2 of the Summer 2013 final exam.
-- Previously, with overlap (P_OVERLAP <= '1';) or without overlap (P_OVERLAP <= '0';), 
-- we were not using this opportunity.
-- Previously we were doing the following:
--   	PLEASE_PRODUCE <= '1' when (PREFETCHED = '0') else '0';
-- Now, we are doing the following:
--   	PLEASE_PRODUCE <= '1' when ((PREFETCHED = '0') or ( (PREFETCHED = '1') and (full = '0') ) ) else '0';

-- The above can be simplified as shown below but since state diagram wise the above easily maps to 
-- the actions coming from the two states separately, we will keep the above form.
-- 		PLEASE_PRODUCE <= '1' when ((PREFETCHED = '0') or (full = '0') ) else '0'; 	
----------------------------------------------------
-- With the improvement, the following results were achieved.
-- # ** Note: Last Data Dispatched !!
-- #    Time: 3660 ns  Iteration: 0  Instance: /tb_interface
-- # ** Note: Last Data Received !!
-- #    Time: 4026 ns  Iteration: 0  Instance: /tb_interface
-- Before this improvement, the following results were achieved.
-- # ** Note: Last Data Dispatched !!
-- #    Time: 3780 ns  Iteration: 0  Instance: /tb_interface
-- # ** Note: Last Data Received !!
-- #    Time: 4114 ns  Iteration: 0  Instance: /tb_interface
----------------------------------------------------
-- This file is created by making a copy of ee560_2_reg_interface_2_loc_FIFO_with_prefetch.vhd and modifying
-- Previous design did not take into account the possibility of prefetching
-- Now we need to have a 2-state state machine to perform prefetching on producer side.

--  1 register INTERFACE design, treating it as a 1-location FIFO
--
--	Designed by Gandhi Puvvada	
--	Date: 7/29/2010, 7/21/2011, 7/28/2011, 7/23/2013
----------------------------------------------------
-- Here the WP and RP pointers were defined as 1-bit pointers. 
-- Here, the a_reg, forms the shallow FIFO reg array.
-- 
-- Here we did not take advantage of "OVERLAP" in the producer or the consumer.
-- The next design 
-- ee560_1_reg_interface_1_loc_FIFO_with_prefetch_and_overlap.vhd 
-- will take this advantage.
----------------------------------------------------
----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal wenq, renq : std_logic;
	signal full, empty: std_logic;
	signal wp, wp_s, wp_ss: std_logic;
	signal rp, rp_s, rp_ss: std_logic;
	signal PREFETCHED: std_logic; -- new FF added to support prefetching 
	signal please_consume_i: std_logic;
	signal a_reg: std_logic_vector(3 downto 0);
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;

	signal wcount, rcount: std_logic_vector(3 downto 0); -- These are for debugging purpose only

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded interface design

----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;

----  Begin Control Signal generation ---------------------
	
	-- PLEASE_PRODUCE <= '1' when ( full = '0' ) else '0'; -- before prefetch operation inclusion
	-- PLEASE_PRODUCE <= '1' when (PREFETCHED = '0') else '0'; -- after prefetch operation inclusion but before the August 2013 improvement
	   PLEASE_PRODUCE <= '1' when ((PREFETCHED = '0') or ( (PREFETCHED = '1') and (full = '0') ) ) else '0'; -- with the August 2013 improvement

	wenq <= '1' when ( ( full = '0' ) and ((PRODUCED = '1') OR (PREFETCHED = '1') ) )  else '0';

	
	please_consume_i <= '1' when ( empty = '0' ) else '0';
	renq <= '1' when ( ( empty = '0' ) and (CONSUMED = '1') )else '0';

	PLEASE_CONSUME <= please_consume_i;
	

	
	----  full and empty signals---------------------
	full <= wp XOR rp_ss;
	empty <= wp_ss XNOR rp;
	-- full  <= '1' when (wp XOR rp_ss) = '1')  else '0';
	-- empty <= '1' when (wp_ss XOR rp) = '0')  else '0';
	
----  End Control Signal generation ---------------------
	
----  output the data ---------------------

	DATAOUT <= a_reg when please_consume_i = '1' else (others => 'Z');	


	WP_and_synchronization_of_RP:process(WCLK, w_syn_resetb)
	variable wp_next_var: std_logic_vector (1 downto 0);
	begin
			if( w_syn_resetb = '0') then
				-- wstate <= QW_produce;
				wp <= '0';
				rp_s <= '0'; rp_ss <= '0';
				wcount <= "0000";
				PREFETCHED <= '0';
			elsif( WCLK'event and WCLK = '1') then
				-- wstate <= n_wstate;
				if (wenq = '1') then 
					wp <= not wp; 
					wcount <= wcount + 1;
				end if;
				if ( (PREFETCHED = '0') and(PRODUCED = '1') and (full = '1')) then
					PREFETCHED <= '1';
				end if;
				if ( (PREFETCHED = '1') and (full = '0')) then
					PREFETCHED <= '0';
				end if;
				rp_s <= rp; 
				rp_ss <= rp_s;
			end if;
	end process;


	RP_and_synchronization_of_WP:process(RCLK, r_syn_resetb)
	variable rp_next_var: std_logic_vector (1 downto 0);
	begin
			if( r_syn_resetb = '0') then
				rp <= '0';
				wp_s <= '0'; 
				wp_ss <= '0';
				rcount <= "0000";
			elsif( RCLK'event and RCLK = '1') then
				if (renq = '1') then 
					rp <= not rp; 
					rcount <= rcount + 1;
				end if;
				wp_s <= wp; 
				wp_ss <= wp_s;
			end if;
	end process;
	

----  Data register ---------------------
	
	Data_Reg_a:process(wclk)
	begin
		if( wclk'event and wclk= '1') then
			if (wenq = '1') then
				a_reg <= DATAIN;
			end if;
		end if;
	end process;

end arc;
