----------------------------------------------------
-- File: ee560_1_reg_interface_1_loc_FIFO_no_prefetch.vhd ( improved version of ee560_1_reg_interface_1_loc_FIFO_3_state.vhd)
--	Designed by Gandhi Puvvada	
--	Date: 7/29/2010, 7/22/2011, 7/20/2013, 7/19/2015
----------------------------------------------------
--  1 register INTERFACE design, treating the single register as a single location FIFO

-- Here we take the advantage, which we avoided taking in ee560_1_reg_interface_1_loc_FIFO.vhd. 
-- But we perform double-synchronization to combat metastable behavior.
-- Also notice that, unlike the ee560_1_reg_interface_1_loc_FIFO_3_state.vhd, this design does not support prefetch.
-- Since the FIFO is very shallow, the disadvantage of not supporting the prefetch operation became very significant 
-- masked the other advantages and disadvantages.
----------------------------------------------------

-- Since the PRODUCED signal arrives around 90% of the clock, we still have time to perform 
-- a simple mealy action to write data and increment the WP pointer and the wcount counter.
-- Since the CONSUMED signal arrives around 90% of the clock, we still have time to perform 
-- a simple mealy action to increment the RP pointer and the rcount counter.

-- The above are all synchronous operations (unlike the asynchronous presets/resets needed 
-- in our earlier design where gated-clock pulses of 50% clock width are needed). 
-- We did not take this advantage* in ee560_1_reg_interface_1_loc_FIFO.vhd (and ended up 
-- having the state machines essentially the same as in the non-FIFO implementation. 
-- Hence we got the same result as before. 
-- The only difference was the way we derived the Empty/Full condition.
-- Now, in this improved design, we take that advantage and get better results as stated below.
-- However, we are still not taking any advantage of pipelined operations in producer/consumer.

-- Note regarding 
--  (a) metastable values after single synchronization, and 
--  (b) not taking mealy operations based on single-synchronized control signals wp_s and rp_s

-- Instead of avoiding only mealy data RTL operations but still taking state transitions based 
-- on these potentially troublesome signals, it is best to perform double-synchronization and 
-- perform both mealy data RTL operations and any state transitions. 

-- We get some advantage because of the mealy operations based on PRODUCED and CONSUMED signals
-- but may lose some advantage because of double synchronization. And in this shallow FIFO, 
-- the major loss is due to not performing the prefetch operation.
----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal wenq, renq : std_logic;
	signal full, empty, wp, wp_s, wp_ss, rp, rp_s, rp_ss: std_logic;
	signal please_consume_i: std_logic;
	signal a_reg: std_logic_vector(3 downto 0);
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;

	signal wcount, rcount: std_logic_vector(3 downto 0); -- These are for debugging purpose only

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded interface design

----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;

----  Begin Control Signal generation ---------------------
	
	PLEASE_PRODUCE <= '1' when ( full = '0' ) else '0';
	wenq <= '1' when ( ( full = '0' ) and (PRODUCED = '1') )else '0';

	
	please_consume_i <= '1' when ( empty = '0' ) else '0';
	renq <= '1' when ( ( empty = '0' ) and (CONSUMED = '1') )else '0';

	PLEASE_CONSUME <= please_consume_i;
	
	----  full and empty signals---------------------
	full <= wp XOR rp_ss;
	empty <= wp_ss XNOR rp;
	
----  End Control Signal generation ---------------------
	
----  output the data ---------------------

	DATAOUT <= a_reg when please_consume_i = '1' else (others => 'Z');
	


	WP_and_synchronization_of_RP:process(WCLK, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				-- wstate <= QW_produce;
				wp <= '0';
				rp_s <= '0'; rp_ss <= '0';
				wcount <= "0000";
			elsif( WCLK'event and WCLK = '1') then
				-- wstate <= n_wstate;
				if (wenq = '1') then 
					wp <= not wp; 
					wcount <= wcount + 1;
				end if;
				rp_s <= rp; 
				rp_ss <= rp_s;
			end if;
	end process;


	RP_and_synchronization_of_WP:process(RCLK, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				rp <= '0';
				wp_s <= '0'; 
				wp_ss <= '0';
				rcount <= "0000";
			elsif( RCLK'event and RCLK = '1') then
				if (renq = '1') then 
					rp <= not rp; 
					rcount <= rcount + 1;
				end if;
				wp_s <= wp; 
				wp_ss <= wp_s;
			end if;
	end process;
	

----  Data register ---------------------

	Data_Reg_a:process(wclk)
	begin
		if( wclk'event and wclk= '1') then
			if (wenq = '1') then
				a_reg <= DATAIN;
			end if;
		end if;
	end process;
	



end arc;
