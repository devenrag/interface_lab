
# --------------------------------------------
# File: ee560_interface_mega_script_r1_all_20.do (99% same as ee560_interface_mega_script_r1.do)
# Author: Gandhi Puvvada 
# Date: 7/27/2011, 7/27/2013
# Parts of this file are from mega_comp by Sabyasachi Ghosh
# --------------------------------------------
#compiles the ee560_mega_tb_interface_r1.vhd
#compiles the interface lab, one design at a time and records the output.
#simulates tb_interface for 8 us

# Procedure
# 0. compile ee560_mega_tb_interface_r1.vhd

# in a loop
# 1. compile next design of interface
# 2. simulate tb_interface for 8 us
# 3.A. compare the ee560_sent_data.txt with the ee560_sent_data_golden.txt
#    If there is a difference, report in the ee560_interface_mega_responses_r1.txt file
#    and 
#	 make a copy of the ee560_sent_data.txt with the design name prefixed and "BAD" suffixed.
# 3.B. compare the ee560_received_data.txt with the ee560_received_data_golden.txt
#    If there is a difference, report in the ee560_interface_mega_responses_r1.txt file
#    and 
#	 make a copy of the ee560_received_data.txt with the design name prefixed and "BAD" suffixed.
# 3.C. compare the ee560_sent_data_with_time.txt with the ee560_sent_data_with_time_golden.txt
#    If there is a difference, report in the ee560_interface_mega_responses_r1.txt file
#    and 
#	 make a copy of the ee560_sent_data.txt with the design name prefixed and "DIFFERENT" suffixed.
# 3.D. compare the ee560_received_data_with_time.txt with the ee560_received_data_with_time_golden.txt
#    If there is a difference, report in the ee560_interface_mega_responses_r1.txt file
#    and 
#	 make a copy of the ee560_received_data.txt with the design name prefixed and "DIFFERENT" suffixed.

# end loop
# --------------------------------------------

# --------------------------------------------
# Open the file in "write" mode, write welcome message and close it. 
# Later the test bench opens the same in "append" mode. 
set outfile [open ee560_interface_mega_responses_r1.txt w]
puts $outfile " "
puts $outfile "EE560 interface lab responses are recorded here. "
puts $outfile "============================================ "
puts $outfile " "
close $outfile
# --------------------------------------------

# --------------------------------------------
# compile ee560_mega_tb_interface_r1.vhd
vcom ee560_mega_tb_interface_r1.vhd

# ; Turn off warnings from the std_logic_arith, std_logic_unsigned
# ; and std_logic_signed packages.
set StdArithNoWarnings 1
# ; Turn off warnings from the IEEE numeric_std and numeric_bit packages.
set NumericStdNoWarnings 1


# 20 idividual lines 
# foreach item {  ee560_0_reg_interface_4way_handshake.vhd   } {   
# foreach item {  ee560_0_reg_improved_interface_4way_handshake.vhd   } {   
# foreach item {  ee560_0_reg_interface_2way_handshake_4states.vhd   } {   
# foreach item {  ee560_0_reg_interface_2way_handshake_2states_one_way.vhd   } {   
# foreach item {  ee560_0_reg_interface_2way_handshake_2states_the_FIFO_way.vhd   } {   

# foreach item {  ee560_1_reg_interface_r1.vhd   } {   
# foreach item {  ee560_1_reg_interface_1_loc_FIFO_3states.vhd   } {   
# foreach item {  ee560_1_reg_interface_1_loc_FIFO_no_prefetch.vhd   } {    
# foreach item {  ee560_1_reg_interface_1_loc_FIFO_with_prefetch.vhd   } {   
# foreach item {  ee560_1_reg_interface_1_loc_FIFO_with_prefetch_and_overlap.vhd   } {   

# foreach item {  ee560_2_reg_interface_r1.vhd   } {   
# foreach item {  ee560_2_reg_interface_2_loc_FIFO_3states.vhd   } {   
# foreach item {  ee560_2_reg_interface_2_loc_FIFO_no_prefetch.vhd   } {    
# foreach item {  ee560_2_reg_interface_2_loc_FIFO_with_prefetch.vhd   } {   
# foreach item {  ee560_2_reg_interface_2_loc_FIFO_with_prefetch_and_overlap.vhd   } {   
# foreach item {  ee560_2_reg_interface_2_loc_FIFO_with_no_prefetch_but_with_overlap.vhd   } {   

# foreach item {  ee560_8_loc_FIFO_no_prefetch.vhd   } {    
# foreach item {  ee560_8_loc_FIFO_with_prefetch.vhd   } {    
# foreach item {  ee560_8_loc_FIFO_with_prefetch_and_overlap.vhd   } {    
# foreach item {  ee560_8_loc_FIFO_with_no_prefetch_but_with_overlap.vhd   } {   

#			
 foreach item {  ee560_0_reg_interface_4way_handshake.vhd ee560_0_reg_improved_interface_4way_handshake.vhd ee560_0_reg_interface_2way_handshake_4states.vhd ee560_0_reg_interface_2way_handshake_2states_one_way.vhd ee560_0_reg_interface_2way_handshake_2states_the_FIFO_way.vhd ee560_1_reg_interface_r1.vhd ee560_1_reg_interface_1_loc_FIFO_3states.vhd ee560_1_reg_interface_1_loc_FIFO_no_prefetch.vhd  ee560_1_reg_interface_1_loc_FIFO_with_prefetch.vhd ee560_1_reg_interface_1_loc_FIFO_with_prefetch_and_overlap.vhd ee560_2_reg_interface_r1.vhd ee560_2_reg_interface_2_loc_FIFO_3states.vhd ee560_2_reg_interface_2_loc_FIFO_no_prefetch.vhd  ee560_2_reg_interface_2_loc_FIFO_with_prefetch.vhd ee560_2_reg_interface_2_loc_FIFO_with_prefetch_and_overlap.vhd ee560_2_reg_interface_2_loc_FIFO_with_no_prefetch_but_with_overlap.vhd ee560_8_loc_FIFO_no_prefetch.vhd  ee560_8_loc_FIFO_with_prefetch.vhd  ee560_8_loc_FIFO_with_prefetch_and_overlap.vhd  ee560_8_loc_FIFO_with_no_prefetch_but_with_overlap.vhd 	} { 







	set outfile [open ee560_interface_mega_responses_r1.txt a]
	puts $outfile " "
	puts $outfile "============================================ "
	# "puts $item" displays the design file name on the modelsim console
	puts $item
	puts $outfile $item
	puts $outfile "============================================ "
	close $outfile
	
	vcom  $item 
	
	# For the final run with all 20 designs, comment out the next 7 lines including the "pause" and uncomment the three lines after that.
	# If you want, you can run this ".do" file in a batch mode at windows command prompt in a command window (cmd.exe).
	# To do so, 
	#		open command window on your PC  (Start => cmd), 
	#		change directory to the interface lab directory (cd C:\Modelsim_projects\ee560_interface_lab) 
	#		invoke modelsim (vsim  -c -quiet  -novopt -nostdout)
	#		run this do file (do ee560_interface_mega_script.do)
	# The batch mode runs quickly than the GUI mode and it does not open and close waveform window.
	
	#	vsim  -novopt  -t 1ps tb_interface 
	#	log -r *
	#	do ee560_tb_interface_r1_wave.do
	#	run 8 us
	#	puts "Inspect your waveform, debug, and then type resume to resume running of the script."
	#	puts "You can also modify the script to suit your debugging needs."
	#	pause
	
	 vsim  -c -quiet  -novopt -nostdout -t 1ps tb_interface 
	 run 8 us 
	 quit -sim
	
	set outfile [open ee560_interface_mega_responses_r1.txt a]

# The following 4 sections are similar. They compare four files with the corresponding golden files.
	
######	
	# Now compare the ee560_sent_data.txt with the ee560_sent_data_golden.txt
	set f1_sent [open ee560_sent_data.txt r]
	set f2_sent [open Golden_files/ee560_sent_data_golden.txt r]

	#get data into a string
	set student_sent_str [read $f1_sent]
	set golden_sent_str [read $f2_sent]
	
	close $f1_sent
	close $f2_sent
	#convert into an array
	set student_sent_array [split $student_sent_str "\n"]
	set golden_sent_array [split $golden_sent_str "\n"]

	set i 1
	set errors_in_sent_data 0
	foreach student_sent_data_item $student_sent_array golden_sent_data_item $golden_sent_array {
		if {$student_sent_data_item != $golden_sent_data_item} {
			puts -nonewline $outfile "Sent data comparison failed for Line number " 
			puts $outfile $i
			incr errors_in_sent_data
		}
		incr i
	}
	if {$errors_in_sent_data != 0} {
		copy ee560_sent_data.txt BAD_files"/"${item}_ee560_sent_data_BAD.txt
		}
	if {$errors_in_sent_data == 0} {
		puts $outfile "Sent all data items correctly."
	}
######

	
######	
	# Now compare the ee560_received_data.txt with the ee560_received_data_golden.txt
	set f1_received [open ee560_received_data.txt r]
	set f2_received [open Golden_files/ee560_received_data_golden.txt r]

	#get data into a string
	set student_received_str [read $f1_received]
	set golden_received_str [read $f2_received]
	
	close $f1_received
	close $f2_received
	#convert into an array
	set student_received_array [split $student_received_str "\n"]
	set golden_received_array [split $golden_received_str "\n"]

	set i 1
	set errors_in_received_data 0
	foreach student_received_data_item $student_received_array golden_received_data_item $golden_received_array {
		if {$student_received_data_item != $golden_received_data_item} {
			puts -nonewline $outfile "Received data comparison failed for Line number " 
			puts $outfile $i
			incr errors_in_received_data
		}
		incr i
	}
	if {$errors_in_received_data != 0} {
		copy ee560_received_data.txt BAD_files"/"${item}_ee560_received_data_BAD.txt
		}
	if {$errors_in_received_data == 0} {
		puts $outfile "Received all data items correctly."
	}
######

######	
	# Now compare the ee560_sent_data_with_time.txt with the ${item}_ee560_sent_data_with_time_golden.txt

  if {$errors_in_sent_data == 0} {
 
	set f1_sent_with_time [open ee560_sent_data_with_time.txt r]
	set f2_sent_with_time [open Golden_files/${item}_ee560_sent_data_with_time_golden.txt r]

	#get data into a string
	set student_sent_with_time_str [read $f1_sent_with_time]
	set golden_sent_with_time_str [read $f2_sent_with_time]
	
	close $f1_sent_with_time
	close $f2_sent_with_time
	#convert into an array
	set student_sent_with_time_array [split $student_sent_with_time_str "\n"]
	set golden_sent_with_time_array [split $golden_sent_with_time_str "\n"]

	set i 1
	set errors_in_sent_data_with_time 0
	foreach student_sent_data_with_time_item $student_sent_with_time_array golden_sent_data_with_time_item $golden_sent_with_time_array {
		if {$student_sent_data_with_time_item != $golden_sent_data_with_time_item} {
			puts -nonewline $outfile "Sent data time comparison failed for Line number " 
			puts $outfile $i
			incr errors_in_sent_data_with_time
		}
		incr i
	}
	if {$errors_in_sent_data_with_time != 0} {
		copy ee560_sent_data_with_time.txt BAD_files"/"${item}_ee560_sent_data_with_time_DIFFERENT.txt
		}
	if {$errors_in_sent_data_with_time == 0} {
		puts $outfile "Sent all data items at right times."
	}
  }  
  if {$errors_in_sent_data != 0} {
	puts $outfile "Sent data times were not checked as there were errors in sent data values."
	   }  
######

	
######	
  if {$errors_in_received_data == 0} {
  
	# Now compare the ee560_received_data_with_time.txt with the ${item}_ee560_received_data_with_time_golden.txt
	set f1_received_with_time [open ee560_received_data_with_time.txt r]
	set f2_received_with_time [open Golden_files/${item}_ee560_received_data_with_time_golden.txt r]

	#get data into a string
	set student_received_with_time_str [read $f1_received_with_time]
	set golden_received_with_time_str [read $f2_received_with_time]
	
	close $f1_received_with_time
	close $f2_received_with_time
	#convert into an array
	set student_received_with_time_array [split $student_received_with_time_str "\n"]
	set golden_received_with_time_array [split $golden_received_with_time_str "\n"]

	set i 1
	set errors_in_received_data_with_time 0
	foreach student_received_data_with_time_item $student_received_with_time_array golden_received_data_with_time_item $golden_received_with_time_array {
		if {$student_received_data_with_time_item != $golden_received_data_with_time_item} {
			puts -nonewline $outfile "Received data time comparison failed for Line number " 
			puts $outfile $i
			incr errors_in_received_data_with_time
		}
		incr i
	}
	if {$errors_in_received_data_with_time != 0} {
		copy ee560_received_data_with_time.txt BAD_files"/"${item}_ee560_received_data_with_time_DIFFERENT.txt
		}
	if {$errors_in_received_data_with_time == 0} {
		puts $outfile "Received all data items at right times."
	}
  }
  if {$errors_in_received_data != 0} {
	puts $outfile "Received data times were not checked as there were errors in received data values."
  }
######

	
	puts $outfile "============================================ "
	puts $outfile " "
	puts $outfile " "
	close $outfile

}
# --------------------------------------------
