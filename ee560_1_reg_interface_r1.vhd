----------------------------------------------------
-- File: ee560_1_reg_interface_r1.vhd 
--  1 register INTERFACE design
--
--	Designed by Gandhi Puvvada and Lung-Sheng Chen	
--	Date:12/1/99, 7/20/2006 (file name added), 7/21/2011 (a_write and a_read are made active-high)
----------------------------------------------------

----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal a_write, a_read, please_consume_i: std_logic;
	signal a_reg: std_logic_vector(3 downto 0);
	signal g_rclk, g_wclk, a_filled, a_filled_s, a_empty, a_empty_s: std_logic;
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;
	signal ae_presetb, ae_s_presetb, af_presetb: std_logic;

	type W_state_enum is ( QW_produce, QW_filled, QW_wait);
	type R_state_enum is ( QR_consume, QR_read, QR_wait);
	signal wstate, n_wstate : W_state_enum;
	signal rstate, n_rstate: R_state_enum;

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded inerface design

----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;


----  State Control---------------------

	W_State_Ctrl: process(PRODUCED, wstate, a_filled_s)
	begin
			n_wstate <= wstate;
			a_write <= '0'; -- high-active signal inactivated by default
			PLEASE_PRODUCE <= '0';

			case wstate is  
				when QW_produce =>
							PLEASE_PRODUCE <= '1';
							if( PRODUCED = '1' and a_filled_s = '0') then 
								n_wstate <= QW_filled;
							elsif( PRODUCED ='1' and a_filled_s = '1') then
								n_wstate <= QW_wait;
							end if;
				when QW_wait =>
							if( a_filled_s = '0' ) then
								n_wstate <= QW_filled; 
							end if;
				when QW_filled =>
							a_write <= '1';
							n_wstate <= QW_produce;
				when others => null;
			end case;
	end process;


	W_State_Reg:process(WCLK, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				wstate <= QW_produce;
			elsif( WCLK'event and WCLK = '1') then
				wstate <= n_wstate;
			end if;
	end process;


	R_State_Ctrl:process(CONSUMED, rstate, a_empty_s)
	begin
			n_rstate <= rstate;
			a_read <= '0'; -- high-active signal inactivated by default
			please_consume_i <= '0';
			
			case rstate is 
				when QR_wait =>
							if( a_empty_s ='0') then 
								n_rstate <= QR_consume;
							end if;
				when QR_consume=>
							please_consume_i <= '1';
							if( CONSUMED = '1') then
								n_rstate <= QR_read;
							end if;
				when QR_read =>
							a_read <= '1';
							n_rstate <= QR_wait;
				when others => null;
			end case;
	end process;
	

	R_State_Reg:process(RCLK, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				rstate <= QR_wait;
			elsif( RCLK'event and RCLK = '1') then
				rstate <= n_rstate;
			end if;
	end process;
	

----  Gated Clock---------------------
	
	g_wclk <= not ( a_write and (not WCLK) );
	g_rclk <= not ( a_read  and (not RCLK) );
	
----  Data registers---------------------

	Data_Reg_a:process(g_wclk)
	begin
		if( g_wclk'event and g_wclk= '1') then
			a_reg <= DATAIN;
		end if;
	end process;

----  handshaking signals---------------------

	ae_presetb <= g_rclk and w_syn_resetb;
	
	A_empty_reg:process(g_wclk, ae_presetb)
	begin
			if( ae_presetb = '0') then
				a_empty <= '1';
			elsif( g_wclk'event and g_wclk= '1') then
				a_empty <= '0';
			end if;
	end process;
	
	ae_s_presetb <= g_rclk and r_syn_resetb;
	
	A_empty_s_reg:process(RCLK, ae_s_presetb)
	begin
			if( ae_s_presetb = '0') then
				 a_empty_s <= '1';
			elsif( RCLK'event and RCLK= '1') then
				 a_empty_s <= a_empty;
			end if;
	end process;


	af_presetb <= g_wclk;

	A_filled_reg:process(g_rclk, af_presetb, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				a_filled <= '0';	
			elsif( af_presetb = '0') then
				 a_filled <= '1';
			elsif( g_rclk'event and g_rclk= '1') then
				a_filled <= '0';
			end if;
	end process;
	
	
	A_filled_s_reg:process(WCLK, af_presetb, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				a_filled_s <= '0';	
			elsif( af_presetb = '0') then
				a_filled_s <= '1';
			elsif( WCLK'event and WCLK= '1') then
				 a_filled_s <= a_filled;
			end if;
	end process;

----  output signals---------------------
	
	PLEASE_CONSUME <= please_consume_i;

	DATAOUT <= a_reg when please_consume_i = '1' else (others => 'Z');

end arc;
