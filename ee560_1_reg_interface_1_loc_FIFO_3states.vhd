----------------------------------------------------
-- File: ee560_1_reg_interface_1_loc_FIFO_3states.vhd (based on the earlier ee560_1_reg_interface_r1.vhd) 
--  1 register INTERFACE design, treating the single register as a single location FIFO
--
--	Designed by Gandhi Puvvada	
--	Date: 7/29/2010, 7/23/2011, 7/22/2013
----------------------------------------------------
-- The earlier design, using gated clock pulses to asynchronously preset flip-flops in the other 
-- clock's domain, is more of an exercise for students. It is not a good design because of the 
-- following reasons.
--  	1. Asynchronous clears and presets are DFT (Design For Test) violations.
--		2. To produce a gated pulse for the asynchronous operation, we ended up needing an extra clock 
--		   	as the PRODUCED and CONSUMED signals arrive around 90% of the clock. The gated pulses
--			are produced during the later half of the clock and require that the control signal 
--			settles down before 50% of the clock. In FIFO design methodology, you do not set or preset
--			asynchronously and hence you do not need 50%-clock-wide gated_clock pulses. So we can perform
--  	   	mealy operations. However, we are not taking this advantage of performing mealy operations* 
--			yet here in this code, as we want to demonstrate that it is possible to do what was done in
--			ee560_1_reg_interface_r1.vhd, using the FIFO method. We will get the exact same results.
--		3. For one register or two registers, one can think of individual control signals 
--			(like the A_empty and A_filled in the ee560_1_reg_interface_r1.vhd), but very soon
--			for 8 registers or more, we will definitely use a FIFO structure. So, why don't we use
--			the two-clock fifo design methodology for this 1-location fifo?
-- Students may get intrigued about how to use a 0-bit or 1-bit or 2-bit counters, 
-- how to derive the full and empty conditions, whether gray-code counter is different compared
-- to a binary counter, if it is as small as a 1-bit counter, whether single synchronization or
-- double synchronization is needed to emulate the previous design, whether there is any improvement
-- or reduction in efficiency because of using the FIFO methodology.
----------------------------------------------------	
-- Design solution:
-- For a 16-location fifo, we need at minimum a 4-bit write pointer/counter and a 4-bit read pointer/counter.
-- But we normally go for 5-bit pointers/counters, so that, the 5-bit depth can represent the 17 depth values 
--(0 to 16).
-- Similarly, for a 1-location fifo, we go for a 1-bit counter, so the two depth values (0 or 1) can be represented 
-- by the 1-bit depth. We need to do mod_2 subtraction of one bit, which is nothing but a single XOR gate.
-- depth = WP XOR RP. Notice that, in an earlier 0_reg design, ee560_0_reg_interface_2way_handshake_2states.vhd, 
-- we produced change_in_take and change_in_got using XOR gates. Though it is not identical, it is similar.
-- In a single location FIFO, the FIFO is either EMPTY or FULL (one or the other all the time).
-- In the case of a FIFO, we produce two depths: w_depth and r_depth.
-- w_depth <= wp - rp_s;  r_depth <= wp_s - rp;
-- The above means that we are doing 1-level of synchronizing. But we are forewarned that rp_s and wp_s can go into 
-- metastable condition and would come out of it around 70% of the clock. 
-- We were told not to take any mealy operation based on these signals.
-- Since the PRODUCED or CONSUMED signal arrives around 90% 
-- of the clock, we still have time to perform a mealy action to read or write data and increment the counter
-- as these are all synchronous operations (unlike the asynchronous presets needed in our earlier design 
-- where gated_clock pulses of 50% clock width need to be produced). However, we are not taking that advantage*
-- so that we focus on replacing the flag bits (A_empty, A_filled) with fifo pointers.
-- So, the state machines are essentially the same as before. Empty/Full condition derivation is different here.

----------------------------------------------------
-- *Exact same response as we did not take the advantage stated above.
-- (i.e. single-synchronization and no mealy actions)
-- Last data was dispatched by the Producer ! Time = 4200 ns
-- Last data was received by the Consumer !! Time = 4554 ns
----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity interface is 
	port( 
		RESETB, WCLK, RCLK: in std_logic;
		PRODUCED, CONSUMED: in std_logic;
		DATAIN: in std_logic_vector(3 downto 0);
		DATAOUT: out std_logic_vector( 3 downto 0);
		PLEASE_PRODUCE, PLEASE_CONSUME: out std_logic;
		P_OVERLAP, C_OVERLAP: out std_logic
	);
end interface;


architecture arc of interface is

	signal wenq, renq : std_logic;
	signal full, empty, wp, wp_s, rp, rp_s: std_logic;
	signal please_consume_i: std_logic;
	signal a_reg: std_logic_vector(3 downto 0);
	-- signal g_rclk, g_wclk, a_filled, a_filled_s, a_empty, a_empty_s: std_logic;
	signal w_syn_resetb, r_syn_resetb,wclk_resetb, rclk_resetb : std_logic;

	type W_state_enum is ( QW_produce, QW_filled, QW_wait);
	type R_state_enum is ( QR_consume, QR_read, QR_wait);
	signal wstate, n_wstate : W_state_enum;
	signal rstate, n_rstate: R_state_enum;

begin

	P_OVERLAP <= '0'; C_OVERLAP <= '0'; -- simple-minded inerface design

----  Synchronous reset signal generation

	Wclk_syn_resetb:process(WCLK)
	begin
		if( WCLK'event and WCLK ='1') then
			wclk_resetb <= RESETB;
		end if;
	end process;

	w_syn_resetb <= wclk_resetb and RESETB;
	
	
	Rclk_syn_resetb:process(RCLK)
	begin
		if( RCLK'event and RCLK ='1') then
			rclk_resetb <= RESETB;
		end if;
	end process;

	r_syn_resetb <= rclk_resetb and RESETB;


----  State Control---------------------

	W_State_Ctrl: process(PRODUCED, wstate, full)
	begin
			n_wstate <= wstate;
			wenq <= '0'; -- high active
			PLEASE_PRODUCE <= '0';

			case wstate is  
				when QW_produce =>
							PLEASE_PRODUCE <= '1';
							if( PRODUCED = '1' and full = '0') then 
								n_wstate <= QW_filled;
							elsif( PRODUCED ='1' and full = '1') then
								n_wstate <= QW_wait;
							end if;
				when QW_wait =>
							if( full = '0' ) then
								n_wstate <= QW_filled; 
							end if;
				when QW_filled =>
							wenq <= '1';
							n_wstate <= QW_produce;
				when others => null;
			end case;
	end process;


	W_State_Reg:process(WCLK, w_syn_resetb)
	begin
			if( w_syn_resetb = '0') then
				wstate <= QW_produce;
				wp <= '0';
				rp_s <= '0';
			elsif( WCLK'event and WCLK = '1') then
				wstate <= n_wstate;
				if (wenq = '1') then wp <= not wp; end if;
				rp_s <= rp;
			end if;
	end process;


	R_State_Ctrl:process(CONSUMED, rstate, empty)
	begin
			n_rstate <= rstate;
			renq <= '0'; -- high active
			please_consume_i <= '0';
			
			case rstate is 
				when QR_wait =>
							if( empty ='0') then 
								n_rstate <= QR_consume;
							end if;
				when QR_consume=>
							please_consume_i <= '1';
							if( CONSUMED = '1') then
								n_rstate <= QR_read;
							end if;
				when QR_read =>
							renq <= '1';
							n_rstate <= QR_wait;
				when others => null;
			end case;
	end process;
	

	R_State_Reg:process(RCLK, r_syn_resetb)
	begin
			if( r_syn_resetb = '0') then
				rstate <= QR_wait;
				rp <= '0';
				wp_s <= '0';
			elsif( RCLK'event and RCLK = '1') then
				rstate <= n_rstate;
				if (renq = '1') then rp <= not rp; end if;
				wp_s <= wp;
			end if;
	end process;
	

----  Data register ---------------------

	Data_Reg_a:process(wclk)
	begin
		if( wclk'event and wclk= '1') then
			if (wenq = '1') then
				a_reg <= DATAIN;
			end if;
		end if;
	end process;
	
----  full and empty signals---------------------
	full <= wp XOR rp_s;
	empty <= wp_s XNOR rp;
	
----  output signals---------------------
	
	PLEASE_CONSUME <= please_consume_i;

	DATAOUT <= a_reg when please_consume_i = '1' else (others => 'Z');

end arc;
